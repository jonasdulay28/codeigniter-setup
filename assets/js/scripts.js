$(document).ready(function()
{
    function scroll()
    {
        if ($(window).scrollTop() > 60) {
            $(".header-middle").addClass("navbar-fixed-top",500,"easeInBack");
            $(".promo").addClass("none",15000,"easeInBack");
            $(".mainmenu").addClass("stick_main_menu",15000,"easeInBack");
        }                                          
        else
        {
            $(".header-middle").removeClass("navbar-fixed-top",1000, "easeInBack");
            $(".promo").removeClass("none");
            $(".mainmenu").removeClass("stick_main_menu");
            $(".mainmenu").addClass("stick_main_menu2",15000,"easeInBack");
        }
    }
    document.onscroll = scroll;
});

    function notify(mes,mes_type)
    {
        $.notify({
        // options
          message: mes 
        },{
          // settings
          type: mes_type,
          placement: {
            from: "bottom",
            align: "right"
          },
          delay:3000,
          animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
          },
      });
    }

    function notify5(mes,mes_type,link)
    {
        $.notify({
        // options
          message: mes,
          url: link
        },{
          // settings
          type: mes_type,
          placement: {
            from: "bottom",
            align: "right"
          },
          delay:5000,
          animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
          },
      });
    }

    function notify4(mes,mes_type)
    {
        $.notify({
        // options
          message: mes 
        },{
          // settings
          type: mes_type,
          placement: {
            from: "bottom",
            align: "right"
          },
          delay:5000,
          animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
          },
      });
    }

    function notify2(header,mes,mes_type)
    {
        swal(
          header,
          mes,
          mes_type
        )
    }

    function notify3(header,mes,mes_type)
    {
      swal({
        title: header,
        text: mes,
        type: mes_type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm'
      }).then(function () {
        return "wew";
      })
    }


    function verify_acc_modal(token,oauth_uid)
    {
        swal({
          title: 'Please wait for the account verification and enter the verification code on your email.',
          input: 'text',
          showCancelButton: false,
          confirmButtonText: 'Submit',
          showLoaderOnConfirm: true,
          preConfirm: function (text) {
            return new Promise(function (resolve, reject) {
              setTimeout(function() {
                var post_url = '../Authentication/verify_key';
                $.ajaxSetup({data: {token: token}});
                $.ajax({
                    type : 'POST',
                    url : post_url,
                    data: {oauth_uid: oauth_uid, key: text},
                    dataType:"json",
                    beforeSend:function(){
                    },
                    success : function(res){
                        if(res.message=="success")
                        {
                            resolve();
                            $('#register_form')[0].reset();
                            setTimeout(function() {
                              window.location.href= res.url;
                          }, 2000);
                        }
                        else
                        {
                            $('#register_form')[0].reset();
                            $('#loading_reg').html('');
                            $('.reg_btn').show(500);
                            $('input:hidden[name="token"]').val(res.token);
                            CFG.token  = res.token;
                            $.ajaxSetup({data: {token: res.token}});
                            reject('Your enter invalid code or the token has been expired. Please try again.');
                        }
                                                
                    },
                    error : function(res) {
                         console.log(res);
                    }
                });
              }, 3000)
            })
          },
          allowOutsideClick: false
        }).then(function (text) {
          swal({
            type: 'success',
            title: 'Account verified!',
            html: 'Please wait. You are now redirecting to the dashboard.'
          })
        })
    }

    function forgot_password_modal(token)
    {
        swal({
          title: 'Please enter the your email.',
          input: 'email',
          showCancelButton: false,
          confirmButtonText: 'Submit',
          inputPlaceholder: 'Enter your email address',
          showLoaderOnConfirm: true,
          preConfirm: function (email) {
            return new Promise(function (resolve, reject) {
              setTimeout(function() {
                var post_url = '../Authentication/send_forgot_password';
                $.ajaxSetup({data: {token: token}});
                $.ajax({
                    type : 'POST',
                    url : post_url,
                    data: {email: email},
                    dataType:"json",
                    beforeSend:function(){
                    },
                    success : function(res){
                      if(res.url)
                      {
                        console.log(res);
                         window.location.href= res.url;
                       
                      }
                      else
                      {
                        if(res.message=="success")
                        {
                          console.log(res);
                          $('input:hidden[name="token"]').val(res.token);
                          CFG.token  = res.token;
                          $.ajaxSetup({data: {token: res.token}});
                          resolve();
                        }
                        else
                        {
                          console.log(res);
                            $('input:hidden[name="token"]').val(res.token);
                            CFG.token  = res.token;
                            $.ajaxSetup({data: {token: res.token}});
                            reject(res.message);
                        }
                      }
                    },
                    error : function(res) {
                         console.log(res);
                    }
                });
              }, 3000)
            })
          },
          allowOutsideClick: false,
          showCancelButton: true,
        }).then(function (text) {
          swal({
            type: 'success',
            title: 'Reset Password Successful',
            html: 'Reset password link was successfully sent. <br> Please check your email'
          })
        })
    }
      (function ($) {
    $.extend({
        playSound: function () {
            return $(
                   '<audio class="sound-player" autoplay="autoplay" style="display:none;">'
                     + '<source src="' + arguments[0] + '" />'
                     + '<embed src="' + arguments[0] + '" hidden="true" autostart="true" loop="false"/>'
                   + '</audio>'
                 ).appendTo('body');
        },
        stopSound: function () {
            $(".sound-player").remove();
        }
    });
})(jQuery);

