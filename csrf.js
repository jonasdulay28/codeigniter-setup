function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

$(function($) {
    // this bit needs to be loaded on every page where an ajax POST may happen
    $.ajaxSetup({
        data: {
            'csrf_pdfrun': getCookie('csrf_cookie_pdfrun')
        }
    });
    // now you can use plain old POST requests like always
});